﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PaddleAI : MonoBehaviour {

	// Use this for initialization
	private Rigidbody rigidbody;
	public float speed = 5f;
	public Transform puck;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}

	void FixedUpdate () {
		Vector3 pos = new Vector3(4f + puck.position.x/3.2f, puck.position.y);
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;

		// check is this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
