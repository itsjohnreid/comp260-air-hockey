﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddleWASD : MonoBehaviour {

	private Rigidbody rigidbody;
	public float maxSpeed = 7f; // in metres per second
	public float acceleration = 5f; // in metres/second/second
	public Vector3 vel = Vector3.zero;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}
		
	void FixedUpdate () {
		float xmovement = Input.GetAxis ("Horizontal");
		float ymovement = Input.GetAxis ("Vertical");

		if (xmovement > 0) {
			vel = vel + Vector3.right * acceleration;
			if (vel.x > maxSpeed) {
				vel.x = maxSpeed;
			}
		}
		if (xmovement < 0) {
			vel = vel + Vector3.left * acceleration;
			if (vel.x < -maxSpeed) {
				vel.x = -maxSpeed;
			}
		}
		if (ymovement > 0) {
			vel = vel + Vector3.up * acceleration;
			if (vel.y > maxSpeed) {
				vel.y = maxSpeed;
			}
		}
		if (ymovement < 0) {
			vel = vel + Vector3.down * acceleration;
			if (vel.y < -maxSpeed) {
				vel.y = -maxSpeed;
			}
		}

		if (xmovement == 0) {
			vel.x = 0;
		}
		if (ymovement == 0) {
			vel.y = 0;
		}

		rigidbody.velocity = vel;
	}
}
